/**
 * @file Serverless Sample endpoint handler.
 *
 * Organize by modules
 * @module serverless-sample
 *
 * @copyright Adello Inc, 2018
 */

/**
 * GET /hello - get test response
 * @async
 * @public
 *
 * Reference (major) functions in other files
 * @see <filename-name>.exports.<function-name>
 *
 * @param {object}event - The API request.
 * @param {object}context - The lambda context.
 */
export const hello = async (event, context) => {
    return {
        statusCode: 200,
        body: JSON.stringify({
            message: `Go Serverless v1.0! ${await message({
                time: 1,
                copy: "Your function executed successfully!"
            })}`
        })
    };
};

/**
 * Helper function for generating async response
 * @async
 * @private
 *
 * Function parameters
 * @param {Integer}time - Time delay in seconds.
 * @param {object}rest - The rest of the parameters.
 */
const message = ({ time, ...rest }) =>
    new Promise((resolve, reject) =>
        setTimeout(() => {
            resolve(`${rest.copy} (with a delay)`);
        }, time * 1000)
    );
