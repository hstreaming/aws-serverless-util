#!/usr/bin/env bash
# Build ZIP package for lambda deployment

set -eux
OUT_FILE="serverless-nodejs-starter.zip" # Relative to current directory

# Remove output from previous run, if any
cd $(dirname "$0")
rm -rf "$OUT_FILE" dist
mkdir dist

# Build dist and add code to package
umask 000
node_modules/serverless/bin/serverless package --package dist
(
  cd dist
  zip -r "../$OUT_FILE" .
)

echo "Generated '$OUT_FILE'"
