import config from "../config";

const got = require("got");

export async function post(subject, body) {
    const ticket = {
        fields: {
            project: {
                key: config.jira.PROJECT
            },
            summary: subject,
            description: body,
            issuetype: {
                name: "Task"
            },
            priority: {
                name: "Medium"
            }
        }
    };

    try {
        var response = await got.post("/rest/api/2/issue/", {
            baseUrl: config.jira.HOST,
            json: true,
            headers: {
                Authorization: config.jira.AUTH
            },
            body: ticket
        });

        console.log("Created Jira ticket " + response.body.key);
        return response.body.key;
    } catch (error) {
        console.error("Creating Jira ticket: " + error);
        throw error;
    }
}
