import AWS from "aws-sdk";
import config from "../config";

export function call(qname, event, delayInSec = 0) {
    const queueUrl =
        "https://sqs." +
        config.aws.REGION +
        ".amazonaws.com/" +
        config.aws.ACCOUNT +
        "/" +
        qname;
    const sqs = new AWS.SQS({ region: config.aws.REGION });

    var request = {
        QueueUrl: queueUrl,
        DelaySeconds: delayInSec,
        MessageBody: JSON.stringify(event)
    };

    return sqs.sendMessage(request).promise();
}
