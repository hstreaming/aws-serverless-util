import AWS from "aws-sdk";
import config from "../config";

AWS.config.update({ region: config.aws.REGION });
const cognito = new AWS.CognitoIdentityServiceProvider();

export async function register(email, alias, customerId, userId) {
    const username = alias ? alias : email.substring(0, email.indexOf("@"));

    // see https://docs.aws.amazon.com/cognito-user-identity-pools/latest/APIReference/API_AdminCreateUser.html
    var params = {
        UserPoolId: config.cognito.USERPOOL_ID,
        Username: email,
        DesiredDeliveryMediums: ["EMAIL"],
        // MessageAction: 'SUPPRESS',
        // TemporaryPassword: 'password123',
        UserAttributes: [
            {
                Name: "email",
                Value: email
            },
            {
                Name: "name",
                Value: username
            },
            {
                Name: "email_verified",
                Value: "true"
            },
            {
                Name: "custom:customer-id",
                Value: customerId
            },
            {
                Name: "custom:user-id",
                Value: userId
            }
        ]
    };

    return new Promise((resolve, reject) => {
        cognito.adminCreateUser(params, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    });
}

export async function remove(guid) {
    var params = {
        UserPoolId: config.cognito.USERPOOL_ID,
        Username: guid
    };

    return new Promise((resolve, reject) => {
        cognito.adminDeleteUser(params, (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    });
}
