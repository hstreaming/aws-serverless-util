import * as s3Lib from "./s3-lib";
import config from "../config";

// See https://www.npmjs.com/package/ntlm-webapi
var reqNTLM = require("./ntlm-webapi");
var url = require("url");

export function call(odataUrl) {
    // check if loading static records from s3
    if (odataUrl.startsWith("s3:")) {
        var parsedURL = url.parse(odataUrl, true);
        const s3Params = {
            Bucket: parsedURL.host,
            Key: parsedURL.path.slice(1) // drop leading '/'
        };

        return s3Lib
            .call("getObject", s3Params)
            .then(data => {
                return JSON.parse(data.Body);
            })
            .catch(error => {
                console.log(JSON.stringify(error));
            });
    } else {
        // load from on-prem CRM with NTLM authentication
        var request = new reqNTLM({
            url: odataUrl,
            username: config.dynamics.USERNAME,
            password: config.dynamics.PASSWORD
        });

        return request
            .get()
            .then(body => {
                return body;
            })
            .catch(error => {
                console.log(JSON.stringify(error));
            });
    }
}
