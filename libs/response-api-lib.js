export function success(body) {
    return buildResponse(200, body);
}

export function created(body) {
    return buildResponse(201, body);
}

export function nocontent() {
    return buildResponse(204, null);
}

export function failure(body) {
    return buildResponse(500, body);
}

export function missing(body) {
    return buildResponse(400, body);
}

export function unauthorized(body) {
    return buildResponse(401, body);
}

export function notfound(body) {
    return buildResponse(404, body);
}

export function conflict(body) {
    return buildResponse(409, body);
}

export function precondfailed(body) {
    return buildResponse(412, body);
}

export function invalid(body) {
    return buildResponse(422, body);
}

function buildResponse(statusCode, body) {
    return {
        statusCode: statusCode,
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true
        },
        body: body ? JSON.stringify(body) : null
    };
}
