import AWS from "aws-sdk";
import config from "../config";

const s3 = new AWS.S3();

export function call(action, params) {
    return s3[action](params).promise();
}
