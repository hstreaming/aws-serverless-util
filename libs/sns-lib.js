import AWS from "aws-sdk";
import config from "../config";

AWS.config.update({ region: config.aws.REGION });

export function publish(topic, event) {
    const sns = new AWS.SNS();

    var request = {
        TopicArn: topic,
        Message: JSON.stringify(event)
    };

    return sns.publish(request).promise();
}
