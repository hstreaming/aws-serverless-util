import * as AWS from "aws-sdk";
import * as aws4 from "aws4";
import * as got from "got";

import config from "../config";

const EXECUTION_SERVICE_API = "execute-api";
const EXECUTION_SERVICE_S3 = "s3";
const REQUESTS_REGION = config.aws.REGION;

const chain = new AWS.CredentialProviderChain();

// Create a Got instance to use signed requests with APIs
export const gotAWS = got.extend({
    hooks: {
        beforeRequest: [
            opts => {
                return chain.resolvePromise().then(credentials => {
                    opts.region = REQUESTS_REGION;
                    opts.service = EXECUTION_SERVICE_API;
                    aws4.sign(opts, credentials);
                });
            }
        ]
    }
});

// Create a Got instance to use signed requests for S3
export const gotAWSs3 = got.extend({
    hooks: {
        beforeRequest: [
            opts => {
                return chain.resolvePromise().then(credentials => {
                    opts.region = REQUESTS_REGION;
                    opts.service = EXECUTION_SERVICE_S3;
                    aws4.sign(opts, credentials);
                });
            }
        ]
    }
});
