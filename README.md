# Serverless Node.js Starter

A Serverless starter that adds ES7 syntax, serverless-offline, environment variables, and unit test support. Part of the [Serverless Stack](http://serverless-stack.com) guide and enhanced for use in projects inside of Adello.

[Serverless Node.js Starter](https://github.com/AnomalyInnovations/serverless-nodejs-starter) uses the [serverless-webpack](https://github.com/serverless-heaven/serverless-webpack) plugin, [Babel](https://babeljs.io), [serverless-offline](https://github.com/dherault/serverless-offline), and [Jest](https://facebook.github.io/jest/). 

The original [serverless-nodejs-starter](https://github.com/AnomalyInnovations/serverless-nodejs-starter) has been enhanced by Adello with [serverless-domain-manager](https://github.com/amplify-education/serverless-domain-manager), [serverless-plugin-scripts](https://github.com/mvila/serverless-plugin-scripts), and [Jsdoc](http://usejsdoc.org).

It supports:

- **ES7 syntax in your handler functions**
  - Use `import` and `export`
- **Package your functions using Webpack**
- **Run API Gateway locally**
  - Use `serverless offline start`
- **Support for unit tests**
  - Run `npm test` to run your tests
- **Sourcemaps for proper error messages**
  - Error message show the correct line numbers
  - Works in production with CloudWatch
- **Automatic support for multiple handler files**
  - No need to add a new entry to your `webpack.config.js`
- **Add environment variables for your stages**

Changes by Adello:

- **Disabling babel due to large package size**
  - Uncomment `babel-loader` in `webpack.config.js` to re-enable
- **Support for generating 'jsdoc' code documentation**
  - Run `serverless docgen` to generate the documentation
- **Support for generating canonical code format using 'prettier'**
  - Run `serverless pretty` to generate canonical code format
  - Also installed as pre-commit hook in `packages.json`
- **Support for AWS SSL certifcates**
  - Run `serverless create_domain` to request SSL certificate for the service`
- **Common libraries for acessing services using promisses**
  - AWS services, like S3, SQS, SNS, etc using promisses
  - Services requiring AWS4 security header
  - Services requiring NTLM authentication, i.e. Dynamics (Microsoft) 
- **Support for automatic deployment and inmutable deployment artifacts via Jenkins**

---

### Requirements

- [Install the Serverless Framework](https://serverless.com/framework/docs/providers/aws/guide/installation/)
- [Configure your AWS CLI](https://serverless.com/framework/docs/providers/aws/guide/credentials/)

### Installation

To create a new Serverless project.

``` bash
$ serverless install --url https://bitbucket.org/hstreaming/aws-serverless-util --name my-project
```

Enter the new directory

``` bash
$ cd my-project
```

If necessary change/remove the MIT license and adapt the README and package.json description.

Install the Node.js packages

``` bash
$ npm install
```

### Usage

To run unit tests on your local

``` bash
$ npm test
```

To run a function on your local

``` bash
$ serverless invoke local --function hello
```

To simulate API Gateway locally using [serverless-offline](https://github.com/dherault/serverless-offline)

``` bash
$ serverless offline start
```

Run your tests

``` bash
$ npm test
```

We use Jest to run our tests. You can read more about setting up your tests [here](https://facebook.github.io/jest/docs/en/getting-started.html#content).

Generate your documentation

``` bash
$ serverless docgen
```

Generate canonical code format

``` bash
$ serverless pretty
```

Request the SSL certificate for your service. Please note that you will first have to request the certifcate via the AWS Certificate Manager and create a Route53 record for validation (Amazon Route 53 DNS Customers ACM can update your DNS configuration for you.

``` bash
$ serverless create_domain
```

Deploy your project

``` bash
$ serverless deploy
```

Deploy a single function

``` bash
$ serverless deploy function --function hello
```

To add another function as a new file to your project, simply add the new file and add the reference to `serverless.yml`. The `webpack.config.js` automatically handles functions in different files.

To add environment variables to your project

1. Rename `env.example` to `env.yml`.
2. Add environment variables for the various stages to `env.yml`.
3. Uncomment `environment: ${file(env.yml):${self:provider.stage}}` in the `serverless.yml`.
4. Make sure to (-not-) commit your `env.yml`.
